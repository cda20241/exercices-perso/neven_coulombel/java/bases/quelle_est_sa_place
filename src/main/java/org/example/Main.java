package org.example;

import java.util.Scanner;

public class Main {

    public static String adjectifOrdinal(int entier) {
        if (entier == 1) {
            return "' en 1ère position.";
        } else {
            return "' en " + entier + "ème position.";
        }
    }

    public static void main(String[] args) {
        Scanner inputPhrase = new Scanner(System.in);
        String exemplePhrase;
        System.out.println("Veuillez saisir une phrase : ->");
        exemplePhrase = inputPhrase.nextLine();
        Scanner inputCaractere = new Scanner(System.in);
        char exempleCaractere;
        System.out.println("Veuillez saisir un caractère (chiffre ou lettre) à retrouver dans votre phrase ! ->");
        exempleCaractere = inputCaractere.nextLine().charAt(0);

        int positions = 0;
        for (int i = 0; i < exemplePhrase.length(); i++) {
            if (exempleCaractere == exemplePhrase.charAt(i)) {
                System.out.println("On retrouve le caractère '" + exempleCaractere + adjectifOrdinal(i+1));
                positions++;
            }
        }
        if (positions == 0) {
            System.out.println("Le caractère '" + exempleCaractere +  "' n'est pas présent dans la phrase.");
        }
    }
}